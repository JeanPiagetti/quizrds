/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.unipampa.controller;

import br.edu.unipampa.model.Questao;
import com.opencsv.CSVReader;
import com.sun.corba.se.impl.io.InputStreamHook;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import br.edu.unipampa.model.Curiosidade;

/**
 *
 * @author Etapa 4
 */
public class ServidorFinal extends Thread implements Serializable {

    private final String HTTP_VERSION = "HTTP/1.1";
    private Socket client;
    private ArrayList<Questao> questoes;
    private ArrayList<Curiosidade> curiosidades;

    public ServidorFinal(Socket socket) {
        client = socket;
        questoes = new ArrayList<>();
        curiosidades = new ArrayList<>();
        questoes = requisicaoQuestoes("/questoesquiz.csv");
        curiosidades = requisicaoCuriosidades("/infoquiz.csv");
    }

    @Override
    public void run() {
        try {
            System.out.println("Cliente se conectando");
            // client = server.accept();
            System.out.println("Conectado na porta " + client.getLocalPort() + " IP " + client.getInetAddress());
//            ObjectInputStream entrada = new ObjectInputStream(client.getInputStream());
//            String comando = entrada.readUTF();
//            switch (comando) {
//                case "\\quizVariedades":
//                    requisicaoQuestaoAleatoria(new Random().nextInt(11));
//
//                    break;
//                default:
//                    throw new AssertionError();
//            }
            enviaClienteQuestoes();
        } catch (IOException ioe) {

        }
    }

    public void enviaClienteCuriosidades() throws IOException {
        ObjectOutputStream saida = new ObjectOutputStream(client.getOutputStream());
        saida.writeObject(this.curiosidades);
        saida.flush();
        saida.close();
        client.close();
    }

    public void enviaClienteQuestoes() throws IOException {
        ObjectOutputStream saida = new ObjectOutputStream(client.getOutputStream());
        saida.writeObject(this.questoes);
        saida.flush();
        saida.close();
        client.close();
    }

    public String getURIRawContent(String path) throws IOException {
        // Abre a conexão

        Socket cliente;
        cliente = new Socket("quizredes.000webhostapp.com", 80);
        PrintWriter out = new PrintWriter(cliente.getOutputStream(), true);
        BufferedReader in = new BufferedReader(new InputStreamReader(cliente.getInputStream()));
        // Envia a requisição
        // Enviando requisição GET para servidor 
        out.println("GET " + path + " " + HTTP_VERSION);
        out.println("Host: quizredes.000webhostapp.com");
        out.println("Connection: Close");
        out.println();
        boolean loop = true;
        StringBuilder sb = new StringBuilder();
        //Recupera a resposta quando ela estiver disponível
        //Lendo a resposta recuparada do servidor
        while (loop) {
            if (in.ready()) {
                int i = 0;
                while ((i = in.read()) != -1) {
                    sb.append((char) i);
                }
                loop = false;
            }
        }
        return sb.toString();
    }

    public ArrayList<Questao> requisicaoQuestoes(String parametro) {
        try {
            String resposta = getURIRawContent(parametro);
            CSVReader reader = new CSVReader(new BufferedReader(new StringReader(resposta.substring(317))));
            String[] nextLine;
            this.questoes = new ArrayList<>();
            Questao questao;
            while ((nextLine = reader.readNext()) != null) {
                questao = new Questao();
                questao.setTemaQuestao(nextLine[0]);
                questao.setArea(nextLine[1]);
                questao.setNomeQuestao(nextLine[2]);
                questao.setAlternativaA(nextLine[3]);
                questao.setAlternativaB(nextLine[4]);
                questao.setAlternativaC(nextLine[5]);
                questao.setResposta(nextLine[6]);
                this.questoes.add(questao);
            }
            this.questoes.remove(0);

        } catch (IOException ex) {
            Logger.getLogger(ServidorFinal.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return this.questoes;
    }

    public ArrayList<Curiosidade> requisicaoCuriosidades(String parametro) {
        try {
            String resposta = getURIRawContent(parametro);
            CSVReader reader = new CSVReader(new BufferedReader(new StringReader(resposta.substring(317))));
            String[] nextLine;
            curiosidades = new ArrayList<>();
            Curiosidade curiosidade;
            while ((nextLine = reader.readNext()) != null) {
                if (!nextLine[3].equals("id")) {
                    curiosidade = new Curiosidade();
                    curiosidade.setCuriosidade(nextLine[0]);
                    curiosidade.setInformacao(nextLine[1]);
                    curiosidade.setOpcao(nextLine[2]);
                    curiosidade.setId(Integer.parseInt(nextLine[3]));
                    curiosidade.setTema(nextLine[4]);
                    curiosidades.add(curiosidade);
                }
            }
            curiosidades.remove(0);

        } catch (IOException ex) {
            Logger.getLogger(ServidorFinal.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return curiosidades;
    }

    public Questao requisicaoQuestaoAleatoria(int random) {
        return this.questoes.get(random);
    }

    public static void main(String args[]) {
        Socket client;
        ServerSocket server;
        try {
            server = new ServerSocket(3023);
            while (true) {
                System.out.println("Esperando conexão de cliente");
                client = server.accept();
                new ServidorFinal(client).start();
            }
        } catch (IOException ioe) {
            System.out.println(ioe);
        }
    }

}
