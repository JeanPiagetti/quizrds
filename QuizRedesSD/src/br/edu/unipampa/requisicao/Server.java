/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.unipampa.requisicao;

import br.edu.unipampa.model.Curiosidade;
import br.edu.unipampa.model.Questao;
import static br.edu.unipampa.requisicao.HttpRequisicao.HTTP_VERSION;
import com.opencsv.CSVReader;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.StringReader;
import java.io.Writer;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author JPiagetti
 */
public class Server extends Thread {
//    public static void main(String args[]) {
//        try {
//            while (true) {
//                int porta = 3322;
//                ServerSocket server = new ServerSocket(porta);
//                System.out.println("Servidor iniciado na porta " + porta);
//                Socket cliente = server.accept();
//                System.out.println("Cliente conectado do IP " + cliente.getInetAddress().getHostAddress());
//                String strFile = "C:\\Users\\JPiagetti\\Documents\\NetBeansProjects\\QuizRDS\\quizrds\\QuizRedesSD\\Engineering\\questoesquiz.csv";
//                Reader reader = Files.newBufferedReader(Paths.get(strFile));
//                CSVReader csvReader = new CSVReaderBuilder(reader).withSkipLines(1).build();
//                List<String[]> questoes = csvReader.readAll();
//                ObjectOutputStream saida = new ObjectOutputStream(cliente.getOutputStream());
//                saida.flush();
//                System.out.println("Arquivo enviado ao cliente");
//                saida.writeObject(questoes);
//                System.out.println("Encerrando conexao com o cliente");
//                saida.close();
//                cliente.close();
//                server.close();
//            }
//        } catch (IOException ex) {
//            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
//        }

    private static ArrayList<BufferedWriter> clientes;
    private static ServerSocket server;
    private String comando;
    private Socket con;
    private InputStream in;
    private InputStreamReader inr;
    private OutputStream out;
    private BufferedReader bfr;
    private String host = "quizredes.000webhostapp.com";
    private int port = 80;

    public Server(Socket con) {
        this.con = con;
        try {
            in = con.getInputStream();
            inr = new InputStreamReader(in);
            bfr = new BufferedReader(inr);
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
        }
    }

    public String getURIRawContent(String path) throws IOException {
        Socket socket = null;
        // Abre a conexão
        try {
            socket = new Socket(this.host, this.port);
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            // Envia a requisição
            // Enviando requisição GET para servidor 
            out.println("GET " + path + " " + HTTP_VERSION);
            out.println("Host: " + this.host);
            out.println("Connection: Close");
            out.println();
            boolean loop = true;
            StringBuilder sb = new StringBuilder();
            //Recupera a resposta quando ela estiver disponível
            //Lendo a resposta recuparada do servidor
            while (loop) {
                if (in.ready()) {
                    int i = 0;
                    while ((i = in.read()) != -1) {
                        sb.append((char) i);
                    }
                    loop = false;
                }
            }
            return sb.toString();
        } finally {
            if (socket != null) {
                socket.close();
            }
        }
    }

    @Override
    public void run() {
        try {
            //envio da string de resposta para os clientes
            String msg;
            OutputStream ou = this.con.getOutputStream();
            Writer ouw = new OutputStreamWriter(ou);
            BufferedWriter bfw = new BufferedWriter(ouw);
            clientes.add(bfw);
            comando = msg = bfr.readLine();
            // sendToAllCuriosidade(bfw, requisicaoOnlineCuriosidade("/infoquiz.csv"));
            sendToAll(bfw, requisicaoOnline("/questoesquiz.csv"));
            while (!"Sair".equalsIgnoreCase(msg) && msg != null) {

                msg = bfr.readLine();

                System.out.println(msg);
            }
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
        }
    }

    public ArrayList<Curiosidade> requisicaoOnlineCuriosidade(String parametro) {
//        String baseUrl = "quizredes.000webhostapp.com";
//        HttpRequisicao req = new HttpRequisicao(baseUrl, 80);
        ArrayList<Curiosidade> curiosidades = null;
        try {
            String resposta = getURIRawContent(parametro);
            CSVReader reader = new CSVReader(new BufferedReader(new StringReader(resposta.substring(317))));
            String[] nextLine;
            curiosidades = new ArrayList<>();
            Curiosidade curiosidade;
            while ((nextLine = reader.readNext()) != null) {
                if (!nextLine[3].equals("id")) {
                    curiosidade = new Curiosidade();
                    curiosidade.setCuriosidade(nextLine[0]);
                    curiosidade.setInformacao(nextLine[1]);
                    curiosidade.setOpcao(nextLine[2]);
                    curiosidade.setId(Integer.parseInt(nextLine[3]));
                    curiosidade.setTema(nextLine[4]);
                    curiosidades.add(curiosidade);
                }
            }
            curiosidades.remove(0);

        } catch (IOException ex) {
            Logger.getLogger(HttpRequisicao.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return curiosidades;
    }

    public ArrayList<Questao> requisicaoOnline(String parametro) {
        //add coment
        // String baseUrl = "quizredes.000webhostapp.com";
        //  HttpRequisicao req = new HttpRequisicao(baseUrl, 80);
        ArrayList<Questao> questoes = null;
        //add new Trheads
        try {
            String resposta = getURIRawContent(parametro);
            CSVReader reader = new CSVReader(new BufferedReader(new StringReader(resposta.substring(317))));
            String[] nextLine;
            questoes = new ArrayList<>();
            Questao q;
            while ((nextLine = reader.readNext()) != null) {
                q = new Questao();
                q.setTemaQuestao(nextLine[0]);
                q.setArea(nextLine[1]);
                q.setNomeQuestao(nextLine[2]);
                q.setAlternativaA(nextLine[3]);
                q.setAlternativaB(nextLine[4]);
                q.setAlternativaC(nextLine[5]);
                q.setResposta(nextLine[6]);
                questoes.add(q);
            }
            questoes.remove(0);
        } catch (IOException ex) {
            Logger.getLogger(HttpRequisicao.class.getName()).log(Level.SEVERE, null, ex);
        }
        //add coment
        return questoes;
    }

    public void sendToAll(BufferedWriter bwSaida, ArrayList<Questao> msg) throws IOException {
        BufferedWriter bws;
        BufferedWriter ultimo = clientes.get(clientes.size() - 1);

        if (ultimo.equals(clientes.get(clientes.size() - 1))) {
            System.out.println("Sou o último");
//                bws = (BufferedWriter) cliente;
        } else {
            System.out.println("Não sou o último");
        }
//          
        for (BufferedWriter cliente : clientes) {
            cliente.flush();
        }
    }

    public static void main(String[] args) {
        try {
            JLabel message = new JLabel("Porta servidor");
            JTextField portaTxt = new JTextField();
            Object[] textos = {message, portaTxt};
            JOptionPane.showMessageDialog(null, textos);
            server = new ServerSocket(Integer.parseInt(portaTxt.getText()));
            clientes = new ArrayList<>();
            JOptionPane.showMessageDialog(null, "Servidor ativo na porta: " + portaTxt.getText());
            while (true) {
                System.out.println("Aguardando Conexão... " + server.getLocalPort());
                Socket con = server.accept();
                if (con.isConnected()) {
                    System.out.println("Cliente conectado " + con.getInetAddress());
                    Thread t = new Server(con);
                    t.start();
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void sendToAllCuriosidade(BufferedWriter bwSaida, ArrayList<Curiosidade> msg) throws IOException {
        BufferedWriter bws;
        for (BufferedWriter cliente : clientes) {

            bws = (BufferedWriter) cliente;
            cliente.write(msg + "\r\n");
            cliente.flush();
        }
    }
}
