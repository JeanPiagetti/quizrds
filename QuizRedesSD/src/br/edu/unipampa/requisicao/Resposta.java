/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.unipampa.requisicao;

import java.io.Serializable;

/**
 *
 * @author JPiagetti
 */
public class Resposta implements Serializable {

    public final int ARQUIVO_EXISTE = 1;
    public final int ARQUIVO_NAO_ENCONTRADO = 2;
    public final int CONTEUDO_DISPONIVEL = 3;
    public final int EOF = 4;

    private int codResposta;
    private String conteudoResposta;

    /**
     * @return the codResposta
     */
    public int getCodResposta() {
        return codResposta;
    }

    /**
     * @param codResposta the codResposta to set
     */
    public void setCodResposta(int codResposta) {
        this.codResposta = codResposta;
    }

    /**
     * @return the conteudoResposta
     */
    public String getConteudoResposta() {
        return conteudoResposta;
    }

    /**
     * @param conteudoResposta the conteudoResposta to set
     */
    public void setConteudoResposta(String conteudoResposta) {
        this.conteudoResposta = conteudoResposta;
    }

}
