/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.unipampa.requisicao;

/**
 *
 * @author JPiagetti
 */
import br.edu.unipampa.model.Questao;
import com.opencsv.CSVReader;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Server3 {

    private final String HTTP_VERSION = "HTTP/1.1";
    private String host = "quizredes.000webhostapp.com";
    private final int porta = 80;
    private Socket socket;
    //private PrintWriter out;

    public String getURIRawContent(String path) throws IOException {
        // Abre a conexão
        try {
            socket = new Socket(this.host, this.porta);
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            // Envia a requisição
            // Enviando requisição GET para servidor 
            out.println("GET " + path + " " + HTTP_VERSION);
            out.println("Host: " + this.host);
            out.println("Connection: Close");
            out.println();
            boolean loop = true;
            StringBuilder sb = new StringBuilder();
            //Recupera a resposta quando ela estiver disponível
            //Lendo a resposta recuparada do servidor
            while (loop) {
                if (in.ready()) {
                    int i = 0;
                    while ((i = in.read()) != -1) {
                        sb.append((char) i);
                    }
                    loop = false;
                }
            }
            return sb.toString();
        } finally {
            if (socket != null) {
                socket.close();
            }
        }
    }

    public ArrayList<Questao> requisicao(String parametro) {
        ArrayList<Questao> curiosidades = null;
        try {
            String resposta = getURIRawContent(parametro);
            CSVReader reader = new CSVReader(new BufferedReader(new StringReader(resposta.substring(317))));
            String[] nextLine;
            curiosidades = new ArrayList<>();
            Questao questao;
            while ((nextLine = reader.readNext()) != null) {
                questao = new Questao();
                questao.setTemaQuestao(nextLine[0]);
                questao.setArea(nextLine[1]);
                questao.setNomeQuestao(nextLine[2]);
                questao.setAlternativaA(nextLine[3]);
                questao.setAlternativaB(nextLine[4]);
                questao.setAlternativaC(nextLine[5]);
                questao.setResposta(nextLine[6]);
                curiosidades.add(questao);
            }
            curiosidades.remove(0);
        } catch (IOException ex) {
            Logger.getLogger(HttpRequisicao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return curiosidades;
    }

    public static void main(String[] args) {
        Server3 server = new Server3();
        //Declaro o ServerSocket  
        ServerSocket serv = null;

        //Declaro o Socket de comunicação  
        Socket s = null;

        //Declaro o leitor para a entrada de dados  
        BufferedReader entrada = null;
        BufferedWriter saida = null;
        try {
            //Cria o ServerSocket na porta 7000 se estiver disponível  

            serv = new ServerSocket(7000);
            System.out.println("Aguardando conexao " + serv.getLocalPort());
            //Aguarda uma conexão na porta especificada e cria retorna o socket que irá comunicar com o cliente  
            s = serv.accept();
            //Cria um BufferedReader para o canal da stream de entrada de dados do socket s  

            entrada = new BufferedReader(new InputStreamReader(s.getInputStream()));
            saida.write(s, 0, 0);
            // System.out.println("Digite alguma coisa");
            //Aguarda por algum dado e imprime a linha recebida quando recebe  
            System.out.println(entrada.readLine());
            //trata possíveis excessões de input/output. Note que as excessões são as mesmas utilizadas para as classes de java.io    
        } catch (IOException e) {

            //Imprime uma notificação na saída padrão caso haja algo errado.  
            System.out.println("Algum problema ocorreu para criar ou receber o socket.");

        } finally {

            try {

                //Encerro o socket de comunicação  
                s.close();

                //Encerro o ServerSocket  
                serv.close();

            } catch (IOException e) {
            }
        }
    }
}
