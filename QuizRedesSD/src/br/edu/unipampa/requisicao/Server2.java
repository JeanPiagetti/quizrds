/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.unipampa.requisicao;

import br.edu.unipampa.model.Questao;
import com.opencsv.CSVReader;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author JPiagetti
 */
public class Server2 {

    private String host = "quizredes.000webhostapp.com";
    private final int porta = 80;
    private ArrayList<Questao> questoes;
    private Socket socket;
    private final String HTTP_VERSION = "HTTP/1.1";

    public String getURIRawContent(String path) throws IOException {
        // Abre a conexão
        try {
            socket = new Socket(this.host, this.porta);
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            // Envia a requisição
            // Enviando requisição GET para servidor 
            out.println("GET " + path + " " + HTTP_VERSION);
            out.println("Host: " + this.host);
            out.println("Connection: Close");
            out.println();
            boolean loop = true;
            StringBuilder sb = new StringBuilder();
            //Recupera a resposta quando ela estiver disponível
            //Lendo a resposta recuparada do servidor
            while (loop) {
                if (in.ready()) {
                    int i = 0;
                    while ((i = in.read()) != -1) {
                        sb.append((char) i);
                    }
                    loop = false;
                }
            }
            return sb.toString();
        } finally {
            if (socket != null) {
                socket.close();
            }
        }
    }

    public ArrayList<Questao> requisicao(String parametro) {
        ArrayList<Questao> curiosidades = null;
        try {
            String resposta = getURIRawContent(parametro);
            CSVReader reader = new CSVReader(new BufferedReader(new StringReader(resposta.substring(317))));
            String[] nextLine;
            curiosidades = new ArrayList<>();
            Questao questao;
            while ((nextLine = reader.readNext()) != null) {
                questao = new Questao();
                questao.setTemaQuestao(nextLine[0]);
                questao.setArea(nextLine[1]);
                questao.setNomeQuestao(nextLine[2]);
                questao.setAlternativaA(nextLine[3]);
                questao.setAlternativaB(nextLine[4]);
                questao.setAlternativaC(nextLine[5]);
                questao.setResposta(nextLine[6]);
                curiosidades.add(questao);
            }
            curiosidades.remove(0);
        } catch (IOException ex) {
            Logger.getLogger(HttpRequisicao.class.getName()).log(Level.SEVERE, null, ex);
        }
        return curiosidades;
    }

    public void selecionaOp(Socket cliente) {

    }

    public static void main(String[] args) throws IOException {
        Server2 server = new Server2();
        ServerSocket serverSocket = new ServerSocket(25);
        while (true) {
            System.out.println("Aguardando Conexão...");
            Socket cliente = server.socket = serverSocket.accept();
            if (!cliente.getKeepAlive()) {
                System.out.println("Cliente conectado com estado inativo");
            }
        }

    }
}
