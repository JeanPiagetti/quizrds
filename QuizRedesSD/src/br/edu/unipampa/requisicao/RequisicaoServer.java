/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.unipampa.requisicao;

import java.io.*;
import java.net.*;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
//import zmq 

/**
 *
 * @author JPiagetti
 */
public class RequisicaoServer {

    /**
     * Versão do protocolo utilizada
     */
    private String host;
    private int port;

    public RequisicaoServer() {
    }

    /**
     * Construtor do cliente HTTP Realiza uma requisição local
     *
     * @param host host para o cliente acessar
     * @param porta
     * @return List<String[]> para as informacoes
     */
    public List<String[]> requisitaLocal(String host, int porta) {
        List<String[]> informacoes = null;
        Socket cliente;
        try {
            cliente = new Socket(host, porta);
            ObjectInputStream entrada;
            entrada = new ObjectInputStream(cliente.getInputStream());
            informacoes = (List<String[]>) entrada.readObject();
            entrada.close();
            return informacoes;
        } catch (IOException | ClassNotFoundException ex) {
            Logger.getLogger(RequisicaoServer.class.getName()).log(Level.SEVERE, null, ex);
        }
        return informacoes;
    }
    //public void requisita(){ //recebe requisições e reposnde ao cliente

//}
    public static void main(String[] args) {
        RequisicaoServer server = new RequisicaoServer();
//        Scanner entrada = new Scanner(System.in);

//        Random rand = new Random();
//        System.out.println("Digite um comando");
//        String comandoPrincipal = entrada.nextLine();
        String host = "localhost";
        int porta = 3322;
        server.requisitaLocal(host, porta);
//        List<String[]> objeto = server.requisitaLocal(host, porta);
//        for (String[] strings : objeto) {
//            switch (comandoPrincipal) {
//                case "\\curiosidadeComp":
//                    System.out.println("Curiosidade: " + strings[0]);
//                    break;
//                case "\\infoJogo":
//                    System.out.println("Informações do Jogo: " + strings[1]);
//                    break;
//                case "\\opcaoJogo":
//                    System.out.println("Opções de Jogo: " + strings[2]);
//                    break;
//                case "sair":
//                    System.out.println("Saiu");
//                default:
//                    System.out.println("Comando não encontrado");
//                    break;
//            }
//        }
    }
}
