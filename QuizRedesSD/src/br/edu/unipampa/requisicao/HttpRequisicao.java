/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.unipampa.requisicao;

import br.edu.unipampa.model.Curiosidade;
import br.edu.unipampa.model.Questao;
import com.opencsv.CSVReader;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringReader;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

public class HttpRequisicao {

    private ServerSocket serverSocket;
    private String host;
    private int port;
    public final static String HTTP_VERSION = "HTTP/1.1";

    public HttpRequisicao(String host, int port) {
        super();
        this.host = host;
        this.port = port;
    }

    public void tratarConexao() {

    }

    public String getURIRawContent(String path) throws IOException {
        Socket socket = null;
        // Abre a conexão
        try {
            socket = new Socket(this.host, this.port);
            PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
            BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            // Envia a requisição
//                Enviando requisição GET para servidor 
            //Recupera a resposta quando ela estiver disponível
            out.println("GET " + path + " " + HTTP_VERSION);
            out.println("Host: " + this.host);
            out.println("Connection: Close");
            out.println();
            boolean loop = true;
            StringBuilder sb = new StringBuilder();
            //Recupera a resposta quando ela estiver disponível
//                Lendo a resposta recuparada do servidor
            while (loop) {
                if (in.ready()) {
                    int i = 0;
                    while ((i = in.read()) != -1) {
                        sb.append((char) i);
                    }
                    loop = false;
                }
            }
            return sb.toString();
        } finally {
            if (socket != null) {
                socket.close();
            }
        }
    }

    public static ArrayList<Questao> requisicaoOnline(String parametro) {
        //add coment
        String baseUrl = "quizredes.000webhostapp.com";
        HttpRequisicao req = new HttpRequisicao(baseUrl, 80);
        ArrayList<Questao> questoes = null;
        //add new Trheads
        try {
            String resposta = req.getURIRawContent(parametro);
            CSVReader reader = new CSVReader(new BufferedReader(new StringReader(resposta.substring(317))));
            String[] nextLine;
            questoes = new ArrayList<>();
            Questao q;
            while ((nextLine = reader.readNext()) != null) {
                q = new Questao();
                q.setTemaQuestao(nextLine[0]);
                q.setArea(nextLine[1]);
                q.setNomeQuestao(nextLine[2]);
                q.setAlternativaA(nextLine[3]);
                q.setAlternativaB(nextLine[4]);
                q.setAlternativaC(nextLine[5]);
                q.setResposta(nextLine[6]);
                questoes.add(q);
            }
            questoes.remove(0);

        } catch (IOException ex) {
            Logger.getLogger(HttpRequisicao.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
//        add coment
        return questoes;
    }

    public static ArrayList<Curiosidade> requisicaoOnlineCuriosidade(String parametro) {
        String baseUrl = "quizredes.000webhostapp.com";
        HttpRequisicao req = new HttpRequisicao(baseUrl, 80);
        ArrayList<Curiosidade> curiosidades = null;
        try {
            String resposta = req.getURIRawContent(parametro);
            CSVReader reader = new CSVReader(new BufferedReader(new StringReader(resposta.substring(317))));
            String[] nextLine;
            curiosidades = new ArrayList<>();
            Curiosidade curiosidade;
            while ((nextLine = reader.readNext()) != null) {
                if (!nextLine[3].equals("id")) {
                    curiosidade = new Curiosidade();
                    curiosidade.setCuriosidade(nextLine[0]);
                    curiosidade.setInformacao(nextLine[1]);
                    curiosidade.setOpcao(nextLine[2]);
                    curiosidade.setId(Integer.parseInt(nextLine[3]));
                    curiosidade.setTema(nextLine[4]);
                    curiosidades.add(curiosidade);
                }
            }
            curiosidades.remove(0);
        } catch (IOException ex) {
            Logger.getLogger(HttpRequisicao.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
        return curiosidades;
    }

    public static void main(String[] args) {
        for (Curiosidade curiosidade : requisicaoOnlineCuriosidade("/infoquiz.csv")) {
            System.out.println(curiosidade.getCuriosidade());
            System.out.println(curiosidade.getId());
            System.out.println(curiosidade.getInformacao());

        }
    }
}
