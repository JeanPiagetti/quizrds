/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.unipampa.persistencia;

import br.edu.unipampa.model.Questao;

/**
 *
 * @author JPiagetti
 */
interface IDAO {

    public void inserir(Questao r);
        
    public void buscar(Questao r);

    public void deletar(Questao r);
}
