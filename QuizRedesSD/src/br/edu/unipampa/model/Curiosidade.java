/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.unipampa.model;

/**
 *
 * @author JPiagetti
 */
public class Curiosidade {

    private String tema;
    private String curiosidade;
    private int id;
    private String opcao;
    private String informacao;

    public Curiosidade(String tema, String curiosidade, int id, String opcao, String informacao) {
        this.tema = tema;
        this.curiosidade = curiosidade;
        this.id = id;
        this.opcao = opcao;
        this.informacao = informacao;
    }

    public Curiosidade() {
    }

    public String getTema() {
        return tema;
    }

    public void setTema(String tema) {
        this.tema = tema;
    }

    public String getCuriosidade() {
        return curiosidade;
    }

    public void setCuriosidade(String curiosidade) {
        this.curiosidade = curiosidade;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOpcao() {
        return opcao;
    }

    public void setOpcao(String opcao) {
        this.opcao = opcao;
    }

    public String getInformacao() {
        return informacao;
    }

    public void setInformacao(String informacao) {
        this.informacao = informacao;
    }

}
