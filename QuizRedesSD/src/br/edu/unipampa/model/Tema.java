/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.unipampa.model;

/**
 *
 * @author JPiagetti
 */
public class Tema {

    private String tipoTema;
    private String descricaoTema;
    private String nomeTema;

    public Tema(String tipoTema, String descricaoTema, String nomeTema) {
        this.tipoTema = tipoTema;
        this.descricaoTema = descricaoTema;
        this.nomeTema = nomeTema;
    }

    public String getTipoTema() {
        return tipoTema;
    }

    public void setTipoTema(String tipoTema) {
        this.tipoTema = tipoTema;
    }

    public String getDescricaoTema() {
        return descricaoTema;
    }

    public void setDescricaoTema(String descricaoTema) {
        this.descricaoTema = descricaoTema;
    }

    public String getNomeTema() {
        return nomeTema;
    }

    public void setNomeTema(String nomeTema) {
        this.nomeTema = nomeTema;
    }

}
