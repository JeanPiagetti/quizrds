/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.unipampa.model;

/**
 *
 * @author JPiagetti
 */
public class Questao {

    private String temaQuestao;
    private String nomeQuestao;
    private String alternativaA;
    private String alternativaB;
    private String alternativaC;
    private String resposta;
    private String area;

    public Questao() {
    }

    public Questao(String temaQuestao, String nomeQuestao, String alternativaA, String alternativaB, String alternativaC, String resposta, String area) {
        this.temaQuestao = temaQuestao;
        this.nomeQuestao = nomeQuestao;
        this.alternativaA = alternativaA;
        this.alternativaB = alternativaB;
        this.alternativaC = alternativaC;
        this.resposta = resposta;
        this.area = area;
    }

    public String getAlternativaA() {
        return alternativaA;
    }

    public String getArea() {
        return area;
    }

    public void setArea(String area) {
        this.area = area;
    }

    public void setAlternativaA(String alternativaA) {
        this.alternativaA = alternativaA;
    }

    public String getAlternativaB() {
        return alternativaB;
    }

    public void setAlternativaB(String alternativaB) {
        this.alternativaB = alternativaB;
    }

    public String getAlternativaC() {
        return alternativaC;
    }

    public void setAlternativaC(String alternativaC) {
        this.alternativaC = alternativaC;
    }

    public String getNomeQuestao() {
        return nomeQuestao;
    }

    public void setNomeQuestao(String nomeQuestao) {
        this.nomeQuestao = nomeQuestao;
    }

    public String getResposta() {
        return resposta;
    }

    public void setResposta(String resposta) {
        this.resposta = resposta;
    }

    public String getTemaQuestao() {
        return temaQuestao;
    }

    public void setTemaQuestao(String temaQuestao) {
        this.temaQuestao = temaQuestao;
    }

}
