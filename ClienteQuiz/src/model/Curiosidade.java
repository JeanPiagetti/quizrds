/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author JPiagetti
 */
public class Curiosidade {

    private String tema;
    private String descricao;
    private int id;

    public Curiosidade() {
    }

    public Curiosidade(String tema, String descricao, int id) {
        this.tema = tema;
        this.descricao = descricao;
        this.id = id;
    }

    public String getTema() {
        return tema;
    }

    public void setTema(String tema) {
        this.tema = tema;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

}
