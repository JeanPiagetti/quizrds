/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.Socket;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author JPiagetti
 */
public class ClienteConsole {

    private Scanner in;
    private Socket socket;
    private OutputStream ou;
    private OutputStreamWriter ouw;
    private BufferedWriter bfw;

    public ClienteConsole() {

        this.in = new Scanner(System.in);
    }

    public void conectar() throws IOException {
        System.out.println("Digite a porta e o Ip");
        String ip = in.nextLine();
        System.out.println("Ip " + ip);
        int porta = in.nextInt();
        System.out.println("Porta " + porta);
        //in = null;
        socket = new Socket(ip, porta);
        ou = socket.getOutputStream();
        ouw = new OutputStreamWriter(ou);
        bfw = new BufferedWriter(ouw);
        //bfw.write(txtNome.getText() + "\r\n");
        bfw.flush();
        System.out.println("Enviado para o servidor");
    }

    private void desconetar() {

    }

    public void enviarMensagem(String comando) throws IOException {
        if (comando.equals("Sair")) {
            bfw.write("Desconectado \r\n");
            System.out.println("Desconectado \r\n");
        } else {
            bfw.write(comando + "\r\n");

        }
        bfw.flush();
    }

    public void escutar() throws IOException {
        InputStream in = socket.getInputStream();
        InputStreamReader inr = new InputStreamReader(in);
        BufferedReader bfr = new BufferedReader(inr);
        String resposta = bfr.readLine();
        String csv = resposta.substring(317);
        System.out.println("CSV " + csv);
    }

    public void menu() {
        System.out.println("Digite um comando\n");
        System.out.println("Para uma curiosidade Aleatória \\curiosidadeComp");
        System.out.println("Para informações do jogo \\infoJogo");
        System.out.println("Para mostrar as Opções do jogo \\opcaoJogo");
        System.out.println("Para uma questão Aleatória \\quizVariedades");
        System.out.println("Para uma questão específica em uma determinada área\\questaoTEMA");
        System.out.println("Digite SAIR para sair");

        escolha(in.nextLine());
    }

    private void escolha(String comando) {
        switch (comando) {
            case "\\curiosidadeComp":
                System.out.println("Fiz a minha ação");
                pause(2 * 1000);
                menu();
                break;
            case "\\infoJogo":
                System.out.println("Mostrou");
                pause(2 * 1000);

                menu();
                break;
            case "\\opcaoJogo":
                break;
            case "\\quizVariedades":
                break;
            case "\\questaoTEMA":
                System.out.println("Digite uma area a ser buscadas [camadaRede], [modelosArquiteturaRD], [estruturaInternet], [camadaEnlace], [camadaFisica], [camadaAplicacao]");
                break;
            case "sair":
                System.out.println("Saiu!");
                pause(2 * 1000);
                break;

            default:
                System.err.println("Comando errado");
                pause(1 * 1000);
                menu();
        }
    }

    private static void pause(int tempo) {
        try {
            Thread.sleep(tempo);

        } catch (InterruptedException ex) {
            Logger.getLogger(ClienteConsole.class
                    .getName()).log(Level.SEVERE, null, ex);

        }
    }

    public static void main(String[] args) {
        try {
            ClienteConsole main = new ClienteConsole();
            main.conectar();
            
            main.escutar();
//            main.menu();

        } catch (IOException ex) {
            Logger.getLogger(ClienteConsole.class
                    .getName()).log(Level.SEVERE, null, ex);
        }
    }
}
