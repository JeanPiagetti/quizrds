/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.Socket;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author JPiagetti
 */
public class ClienteAgoraVai {

    private Socket socket;
    private OutputStream ou;
    private Writer ouw;
    private BufferedWriter bfw;
    private Scanner in;

    public void conectar() throws IOException {
        in = new Scanner(System.in);
//        System.out.println("Digite o ip e a porta");
        socket = new Socket("127.0.0.1", 12);
        ou = socket.getOutputStream();
        ouw = new OutputStreamWriter(ou);
        bfw = new BufferedWriter(ouw);
        bfw.write("\r\n");
        bfw.flush();
    }

    public void escutar() throws IOException {
        InputStream in = socket.getInputStream();
        InputStreamReader inr = new InputStreamReader(in);
        BufferedReader bfr = new BufferedReader(inr);
        String msg = "";
        while (!"Sair".equalsIgnoreCase(msg)) {
            //if (bfr.ready()) {
            msg = bfr.readLine();
            //  System.out.println(msg);
            if (msg.equals("Sair")) {
                //   texto.append("Servidor caiu! \r\n");
            } else {
                System.out.println(msg + "\r\n");
            }
            //}
        }
    }

    public void sair() throws IOException {
        enviarMensagem("Sair");
        bfw.close();
        ouw.close();
        ou.close();
        socket.close();
    }

    public void enviarMensagem(String msg) throws IOException {
        if (msg.equals("Sair")) {
            bfw.write("Desconectado \r\n");
            System.out.println("Desconectado \r\n");
        } else {
            bfw.write(msg + "\r\n");
            System.out.println(" diz -> " + msg + "\r\n");
        }
        bfw.flush();

    }

    public static void main(String[] args) {
        ClienteAgoraVai main = new ClienteAgoraVai();
        try {
            main.conectar();
            //  main.enviarMensagem("Oi");
            main.escutar();
        } catch (IOException ex) {
            Logger.getLogger(ClienteAgoraVai.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
