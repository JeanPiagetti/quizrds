/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import br.edu.unipampa.model.Curiosidade;
import br.edu.unipampa.requisicao.HttpRequisicao;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import br.edu.unipampa.model.Questao;
import java.util.Random;

/**
 *
 * @author JPiagetti
 */
public class ControllerJogo {

    private String baseUrl = "quizredes.000webhostapp.com";
    private HttpRequisicao requisicao;
    private Random rand;

    public ControllerJogo() {
        rand = new Random();
        requisicao = new HttpRequisicao(baseUrl, 80);
    }

    public void pause(int tempo) {
        try {
            Thread.sleep(tempo);
        } catch (InterruptedException ex) {
            Logger.getLogger(ControllerJogo.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public ArrayList<Questao> retornaQuestoes() {
        return HttpRequisicao.requisicaoOnline("/questoesquiz.csv");
    }

    public ArrayList<Curiosidade> retornaCuriosidades() {
        return HttpRequisicao.requisicaoOnlineCuriosidade("/infoquiz.csv");
    }

    public Curiosidade retornaQuestaoAleatoria(int aleatorio) {
        Curiosidade retorno = null;
        for (Curiosidade object : HttpRequisicao.requisicaoOnlineCuriosidade("/infoquiz.csv")) {
            retorno = object;
        }
        return retorno;
    }
}
