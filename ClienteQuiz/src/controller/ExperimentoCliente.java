/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import br.edu.unipampa.model.Questao;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;

/**
 *
 * @author JPiagetti
 */
public class ExperimentoCliente {

    public static void conecta() {
        try {
            Socket cliente = new Socket("localhost", 3023);
            ObjectInputStream entrada = new ObjectInputStream(cliente.getInputStream());
            ArrayList<Questao> questao = (ArrayList<Questao>) entrada.readObject();
            JOptionPane.showMessageDialog(null, "Alternativa A" + questao.get(0).getAlternativaA());
            JOptionPane.showMessageDialog(null, "Alternativa B" + questao.get(0).getAlternativaB());
            JOptionPane.showMessageDialog(null, "Alternativa C" + questao.get(0).getAlternativaC());
        } catch (IOException ex) {
            Logger.getLogger(ExperimentoCliente.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(ExperimentoCliente.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static void main(String[] args) {
        conecta();
    }
}
